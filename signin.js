const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const User = mongoose.model('User');

exports.authenticate = (username, password) => {
    return new Promise(async (resolve, reject) => {
      try {
        // Get user by username
        const user = await User.findOne({ username });
  
        // Match Password
        bcrypt.compare(password, user.password, (err, isMatch) => {
          if (err) reject;
          if (!isMatch) reject;
          resolve(user);
        });
      } catch (err) {
        // Username not found or password did not match
        reject('Authentication Failed');
      }
    });
};