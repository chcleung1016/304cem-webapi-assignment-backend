const errors = require('restify-errors');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const signin = require('../signin');
const config = require('../config');

module.exports = server => {
    // Signup User
    server.post ('/signup', (req, res, next) => {
        const { name, email, username, password, district } = req.body;
        
        const user = new User({
            name,
            email,
            username,
            password,
            district
        });

        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, async (err, hash) => {
              // Hash Password
              user.password = hash;
              // Save User
              try {
                const newUser = await user.save();
                res.send(201);
                next();
              } catch (err) {
                return next(new errors.InternalError(err.message));
              }
            });
        });
    });

    // Signin User
    server.post('/signin', async (req, res, next) => {
        const { username, password } = req.body;

        try {
            // Authenticate User
            const user = await signin.authenticate(username, password);
            // console.log(user);
            
            const token = jwt.sign(user.toJSON(), config.JWT_SECRET, {
                expiresIn: '60m'
            });
            
            const { iat, exp } = jwt.decode(token);
            // Respond with token
            res.send({ iat, exp, token });

            next();
        } catch (err) {
            // User unauthorized
            return next(new errors.UnauthorizedError(err));
        }
    });
}